
//nice function! from SO via Thilo
function getCookieValue(a) {
	var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
	return b ? b.pop() : '';
}

function insertCsrfIntoAllPostForms() {
	const token = getCookieValue('XSRF-TOKEN');
	if (!token) return;
	for (let form of document.querySelectorAll("form[method='POST']")) {
		form.insertAdjacentHTML('afterbegin', `<input type="hidden" name="_csrf" value="${token}">`);
	}
}

document.addEventListener("DOMContentLoaded", insertCsrfIntoAllPostForms);
