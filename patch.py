from shutil import copyfile

file = "/home/vcap/deps/0/python/lib/python3.6/site-packages/django/contrib/admin/templates/admin/base.html"
patch = "patch.js"

copyfile(file, file + ".bak")

f = open(file, "r")
content = f.read()
f.close()

f = open(patch, "r")
patch = f.read()
f.close()

newcontent = content.replace("</body>", "<script>" + patch + "</script></body>")

f = open(file, "w")
f.write(newcontent)
f.close()

print("Patched file:", file)
